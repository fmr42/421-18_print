Customize paths in print_service.service

Then:
sudo cp print_service.service /lib/systemd/system/
sudo systemctl daemon-reload
sudo systemctl restart print_service.service
sudo systemctl enable print_service.service
sudo journalctl -fu print_service


