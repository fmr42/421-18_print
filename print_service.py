import requests
import json
#import snap7
from snap7.snap7types import areas
import snap7
import time
import numpy as np
print("Initializing...")

def unpad_weintek_string(stringToUnpad):
    stringPadded=stringToUnpad
    while stringPadded[-1]==0:
        stringPadded=stringPadded[0:-1]
    return stringPadded

def print_req(batch,sku):
    print("Sendint print request...")
    url = 'https://rfidtest.gucci.com/gucci-tmr-api/custom/productionOrderRows/leather/externalPrint'
    headers = {'Content-Type': 'application/json',
               'Authorization': 'BasiccHJpbnRBcHBseVVzZXI6cHJpbnRBcHBseTEh'}
    #data = {'commessa': 'EPPV201950500A',
    #        'sku': '8087559387',
    data = {'commessa': batch,
            'sku': sku,
            'qty': '1',
            'printerId': '202005061612-7e5ed4e7-708a-4533-aad5-09d48eeaae52'}
    r = requests.post(url, headers=headers, data=json.dumps(data))
    status= r.status_code
    print(status)
    print(r.content)
    return status

while True:
    try:
        #print("Disconnected.. Trying to connct...")
        db_comm=21
        plc = snap7.client.Client()
        plc.connect('172.16.15.66', 0, 2)
        print("Connected!!!")
        while True:
            try:
                ba=plc.read_area (areas['DB'],db_comm,0,50)
                doi_print= snap7.util.get_bool(ba,0,0)
                #print(doi_print)
                #batch = snap7.util.get_string(ba, 2, 5)
                #sku   = snap7.util.get_string(ba, 22, 5)
                if ( doi_print ):
                    print ("Print request!!!")
                    batch_char_array = ba[10:29]
                    sku_char_array = ba[30:49]
                    batch=unpad_weintek_string(batch_char_array).decode("utf-8")
                    sku=unpad_weintek_string(sku_char_array).decode("utf-8")
                    ret=print_req(batch,sku)
                    #print(batch_char_array[batch_char_array!=0].decode("utf-8"))
                    #print(sku_char_array[batch_char_array!=0].decode("utf-8"))
                    snap7.util.set_bool(ba,0,0,False)
                    snap7.util.set_int(ba,2,ret)
                    plc.write_area(areas['DB'],db_comm,0,ba)
                    print("DONE!")
                #r=plc.read_area(areas['DB'])
                #print(int(r))
                #plc.write_area(area['MK'], 0, 0, 1)
                time.sleep(0.5)
            except:
                print("Reading exeption...")
                time.sleep(1)
                break
    except:
        #print("Connection exeption...")
        time.sleep(1)



